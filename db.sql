CREATE TABLE admin (
  id int(11) NOT NULL,
  username varchar(255) NOT NULL,
  passcode varchar(255) NOT NULL
);

ALTER TABLE admin	  ADD PRIMARY KEY (id);
ALTER TABLE admin	  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

CREATE TABLE produto (
  id int(11) NOT NULL,
  nome varchar(255) NOT NULL,
  descricao varchar(255) NOT NULL,
  valor decimal(15,2)
);

ALTER TABLE produto	  ADD PRIMARY KEY (id);
ALTER TABLE produto	  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;