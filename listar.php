<?php
   include("config.php");
   
   try  {    
    $sql = "SELECT * FROM produto";    
    $statement = $connection->prepare($sql);    
    $statement->execute();
    $result = $statement->fetchAll();
   } catch(PDOException $error) {
     echo $sql . "<br>" . $error->getMessage();
   }
?>

<html>
   
   <head>
      <title>Listar Produto</title>
   </head>
   
   <body>

<?php  
   if ($result && $statement->rowCount() > 0) { ?>
   
   <h2>Produtos</h2>
   
    <table>
      <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th>Descricao</th>
          <th>Valor</th>          
        </tr>
      </thead>
      <tbody>
	  <?php foreach ($result as $row) : ?>
	  <tr>
        <td><?php echo escape($row["id"]); ?></td>
        <td><?php echo escape($row["nome"]); ?></td>
		<td><?php echo escape($row["descricao"]); ?></td>
		<td><?php echo escape($row["valor"]); ?></td>
	  </tr>
	  <?php endforeach; ?>
	  </tbody>
	</table>
	
	<?php } else { ?>
      <blockquote>Sem Items.</blockquote>
    <?php } 
} ?> 

  <a href="welcome.php">Voltar</a>
  </body>
   
</html>