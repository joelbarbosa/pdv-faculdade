<?php
   include("config.php");
   if (isset($_POST['submit'])) {
     try  {
		
		$new_produto = array(
		  "nome" => $_POST['nome'],
		  "descricao"  => $_POST['descricao'],
		  "valor"     => $_POST['valor']
		);
		$sql = sprintf(
		  "INSERT INTO %s (%s) values (%s)",
		  "produto",
		  implode(", ", array_keys($new_produto)),
		  ":" . implode(", :", array_keys($new_produto))
		);
		
		$statement = $db->prepare($sql);
		$statement->execute($new_produto);
	  } catch(PDOException $error) {
		  echo $sql . "<br>" . $error->getMessage();
	  }
   }
?>

<?php if (isset($_POST['submit']) && $statement) : ?>
    <blockquote><?php echo escape($_POST['nome']); ?> Adicionado com sucesso.</blockquote>
<?php endif; ?>

<html>
   
   <head>
      <title>Adicionar Produto</title>
   </head>
   
   <body>

	<h2>Adicionar Produto</h2>

	<form method="post">		
	  <label for="nome">Nome</label>
	  <input type="text" name="descricao" id="descricao">
	  <label for="descricao">Descricao</label>
	  <input type="text" name="descricao" id="descricao">
	  <label for="valor">Valor</label>
	  <input type="number" name="valor" id="valor">
	  <input type="submit" name="submit" value="Submit">
	</form>
	
    <a href="welcome.php">Voltar</a>
	
   </body>
   
</html>